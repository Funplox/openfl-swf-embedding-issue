package ; #if !flash


import openfl._internal.formats.swf.SWFLite;
import openfl.display.MovieClip;
import openfl.utils.Assets;


class UnicornMC extends openfl.display.MovieClip {


	@:keep public var textField (default, null):openfl.text.TextField;
	

	public function new () {

		super ();

		/*
		if (!SWFLite.instances.exists ("khVsLjYX4f7HCctd57gN")) {

			SWFLite.instances.set ("khVsLjYX4f7HCctd57gN", SWFLite.unserialize (Assets.getText ("khVsLjYX4f7HCctd57gN")));

		}
		*/

		var swfLite = SWFLite.instances.get ("khVsLjYX4f7HCctd57gN");
		var symbol = swfLite.symbols.get (5);

		__fromSymbol (swfLite, cast symbol);

	}


}


#else
@:bind @:native("UnicornMC") class UnicornMC extends openfl.display.MovieClip {


	@:keep public var textField (default, null):openfl.text.TextField;
	

	public function new () {

		super ();

	}


}
#end