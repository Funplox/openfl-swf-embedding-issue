package;

import openfl.Assets;
import openfl.display.Sprite;
import openfl.Lib;
import openfl.text.TextFormat;

/**
 * ...
 * @author Creative Magic
 * 
 * In this test you can see that use can embed all kinds of assets (see project.xml) but processed SWFs
 * Fonts, images and sounds are embedded and are accessible just fine.
 */
class Main extends Sprite 
{

	public function new() 
	{
		super();
		
		var assetsList = Assets.list();
		trace(assetsList); // returns the paths of all loaded assets
		
		Assets.loadLibrary("assetPack").onComplete(onLibLoaded).onError(onLibLoadError);
	}
	
	public function addUnicornOnStage():Void
	{
		var dabbingUnicorn = new UnicornMC();
		var textFormat = new TextFormat(Assets.getFont("fonts/Lithos Pro Regular.otf").fontName, 80, 0x00); // this line proves that fonts are embedded just fine. No special loading is needed.
		dabbingUnicorn.textField.defaultTextFormat = textFormat;
		dabbingUnicorn.textField.text = "OK!";
		addChild(dabbingUnicorn);
	}
	
	// EVENT HANDLERS
	
	private function onLibLoaded(_):Void
	{
		trace("LIB LOADED OK");
		addUnicornOnStage();
	}
	
	/**
	* Usually you get a following error:
	* src/Main.hx:47: LIB LOAD ERROR:
	* [IOErrorEvent type="ioError" bubbles=true cancelable=false text="Cannot load file: ./assets/Assets.bundle/swflite.bin" errorID=0]
	* 
	* And then when you try to load the unicorn anyway you get an error too ;(
	**/
	private function onLibLoadError(error:Dynamic):Void
	{
		trace("LIB LOAD ERROR: " + Std.string(error) );
		addUnicornOnStage(); // try adding the unicorn anyway?
	}

}
